﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class IsTriggerDamege : MonoBehaviour
{
    public void OnTriggerEnter(Collider collision)
    {
        if (collision.name == Tags.player)
        {
            var player = collision.GetComponent<PlayerMove>();
            Debug.Log("HIT");
            player.TakeDamege(1);
            player.Move(5);
        }
    }
}
