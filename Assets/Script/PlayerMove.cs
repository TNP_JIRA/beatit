﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerMove : MonoBehaviour
{
    [SerializeField] private Animator anim;
    [SerializeField] private HealthBar healthBar;
    [SerializeField] private CharacterController charContorll;
    [SerializeField] private float speed = 20;
    private Vector3 forward;
    private Vector3 moveDiction;
    private RaycastHit hit;
    private float gravity =-9.81f;
    private int isDeathHash;
    public int maxHealth = 5;
    public int currentHealth = 0;
    //public int currentHp => GetComponent<GameManager>().currentHealth;
    void Start()
    {
        currentHealth = maxHealth;
        healthBar.SetMaxHealth(maxHealth);
        anim = GetComponent<Animator>();
        charContorll = GetComponent<CharacterController>();
        isDeathHash = Animator.StringToHash("isDeath");
    }
    void Update()
    {
        Raycast();
        Move(0);
        if(currentHealth == 0)
        {
            IsDeath();
        }
    }
    public void Move(int slow )
    {
        speed -= slow;
        moveDiction = new Vector3(Input.GetAxis(Tags.horizontalAxis) * speed, gravity, speed);
        charContorll.Move(moveDiction * Time.deltaTime);
        if(speed < 20)
        {
            speed += 2 * Time.deltaTime;
        }
    }
    void IsDeath()
    {
        speed = 0;
        anim.SetBool(isDeathHash, true);
    }
    public void HealHeathBar(int heal)
    {
        currentHealth += heal;
        healthBar.SetHealth(currentHealth);
    }
    public void TakeDamege(int damege)
    {
        Debug.Log("A");
        currentHealth -= damege;
        Debug.Log("B");
        healthBar.SetHealth(currentHealth);
    }
    void Raycast()
    {
        forward = transform.TransformDirection(new Vector3(0, 2, 2));
        Debug.DrawRay(transform.position, forward, Color.red);
        if (Physics.Raycast(transform.position, forward, out hit, 2)) 
        {
            if (hit.collider.name == "Heal") 
            {
                HealHeathBar(1);
                Destroy(hit.collider.gameObject);
            }
            if(hit.collider.name == "Coin")
            {
                Destroy(hit.collider.gameObject);
            }
        }
    }
}
